/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import model.Alumno;
import model.Nombre;
import model.Profesor;
import model.TipoFuncionario;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        Profesor p = null;
        Nombre nombre;
        Alumno a = null;
        int opcion;
        Calendar fechaNac = Calendar.getInstance();
        Calendar fechaRegistro = Calendar.getInstance();
        Calendar horaTutoria = Calendar.getInstance();

        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();

        //CREAR UNA SESION
        Session session = factory.openSession();

        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR PROFESOR
                    nombre = new Nombre("Pepe", "García", "Pérez");
                    p = new Profesor(nombre, TipoFuncionario.INTERINO);
                    System.out.println("Profesor creado con éxito");
                    break;
                case 2://GUARDAR PROFESOR
                    session.beginTransaction();
                    session.save(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor guardado con éxito");
                    break;
                case 3://LEER PROFESOR
                    p = session.get(Profesor.class, 1);
                    System.out.println(p);
                    break;
                case 4://ACTUALIZAR PROFESOR
                    session.beginTransaction();
                    //p.setApe1("Rodríguez");
                    session.update(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor actualizado con éxito");
                    break;
                case 5://ELIMINAR PROFESOR
                    session.beginTransaction();
                    session.delete(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor eliminado con éxito");
                    break;
                case 6://CREAR ALUMNO
                    //(el mes, uno menos)
                    fechaNac.set(1984, 11, 15);
                    horaTutoria.set(2019, 0, 13, 17, 30);
                    a = new Alumno(1, "Juan", 1200, true, fechaNac.getTime(),horaTutoria.getTime(), fechaRegistro.getTime(), "esto son observaciones");
                    System.out.println(a);
                    break;
                case 7://GUARDAR ALUMNO
                    session.beginTransaction();
                    session.save(a);
                    session.getTransaction().commit();
                    System.out.println("Alumno guardado con éxito");
                    break;
                case 8://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 8);

    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR PROFESOR, COMO SON FIJOS DARÁ ERROR");
        System.out.println("1. Crear profesor");
        System.out.println("2. Guardar profesor");
        System.out.println("3. Leer profesor");
        System.out.println("4. Actualizar profesor");
        System.out.println("5. Eliminar profesor");
        System.out.println("6. Crear alumno");
        System.out.println("7. Guardar alumno");
        System.out.println("8. Salir");
    }
}
